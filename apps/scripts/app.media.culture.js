var mediaculture = (function(d, w) {

    //Özel kullanımlar için for prototype methodu
    Object.defineProperty(Object.prototype, 'for', {
        value: function(actionInvoke) {
            for (var key in this) {
                var index = 0;
                //return keyname,currentObj,index
                actionInvoke(key, this[key], index);
                index++;
            }
        }
    });

    var currentPiece = 12;
    //var groups = [];
    //Ekran boyutları
    //Kısa kullanım adı ve maximum alacağı değer belitilmiştir
    //Burada kullanılan kısa tanımlar, ayrıca sınıf isimleri tanımlanırken kullanılacaktır
    //Örnegin mob5 veya mob-5 yada web12 veya web-12 gibi..
    var screenSizes = {
        web: 0,
        tab: 880,
        mob: 660,
        min: 440
    };


    //Eğer geliştirici tarafından özel olarak belirtilmiş boyut değerleri varsa, mevcut değerler ile değiştirilir
    //Sayfa üzerindeki örnek kullanım
    /*
        <script>
        mediaculture.settings({

            web:0,
            mob:500,
            ...,
            ...,
            ...
        });
        </script>
    */

    var settings = (function() {
        try {
            if (mediaCultureSetting) {

                if (mediaCultureSetting.piece)
                    currentPiece = Math.max(mediaCultureSetting.piece);

                for (var n in mediaCultureSetting.sizes) {
                    if (screenSizes.hasOwnProperty(n))
                        screenSizes[n] = mediaCultureSetting.sizes[n];
                }
            }
        } catch (e) {

        }
    })()

    //Tüm sınıf tanımlamalarının yükleneceği sınıf array nesnesi
    var defaults = [];

    //Kullanılacak maptype'in geliştirici tarafından tanımlandığı isim bu array içerinde belirtilmelidir
    var mapTypeName = ['map', 'table', 'grid'];


    //Her bir yeni maptype özelliğini burada tanımlıyoruz
    //Tanımlanan maptype, mapTypeName listesindeki isim ile aynı olmalıdır
    //Oluşturulan yeni maptype için init ve run özellikleri bulunmalıdır
    //init property'si framework çalıştırıldığı anda yüklenmesi gerekiyor ve isteniyorsa bu alan true işaretlenmelidir.
    //Eğer başlangıçta yüklenmeyecekse ve uygulama içerisinde istenilen zamanda çağrılacaksa false işaretlenmelidir
    //Run methodu init true ise başlangıçta otomatik çalıştırılır. 
    //Örneğin, yeni bir tip oluşturdum. Adı "YeniMapTip". Bu adı hem mapTypeName listesine hem de mapTypes nesnesine eklemeliyim
    //Oluşturduğum bu mapTypes özelliğindeki "init" propertisini true işaretlersem, bu sayfa yüklenirken run methodundaki değerlerimin...
    //... sayfa yüklendiği anda oluşturulmasında bir sakınca olmadığı anlamına gelmektedir.
    //... oluşturduğum özelliğin "children" propertisi "in" expression değerini simgeler. root property değeri ise "this" expression değerini simgeler....
    //... eğer children özelliğini true yaparsak, ilgili screenSizes boyutlarında, alt nesneler içindekilerin %'lerden etkilenebilir olduğunu niteler
    //... aynı şekilde root özelliğini true yaparsak, ilgili nesnenin oluşturulmuş % değerlerinden etkilenebilir olduğunu belirtir
    //... örneğin, children özelliği true ise framework şunu yapmaktadır. .YeniMapTip.in[class*='web-12']>* gibi bir yapıya dönüştürmektedir.
    //... aynı şekilde root da benzer özelliğine dönüştürülmektedir.

    var mapTypes = {
        map: {
            init: true,
            children: true,
            root: true,
            run: function() {
                //Map isimli maptype sınıf içeriği tanımlamaları
                defaults.push('.map.in>*, .map.this{float:left; margin-left:0; margin-right:0;}');
                defaults.push('.map.in:before, .map.in::before, .map.in:after, .map.in::after {content: " "; display:block; clear: both;}');
            }
        },
        table: {
            init: true,
            children: false,
            root: true,
            run: function() {
                //Table isimli maptype sınıf içeriği tanımlamaları
                defaults.push('.table.this {display:table; border-spacing:0;} .table.this>*{ display:table-cell; }');
            }
        },
        grid: {
            init: true,
            children: true,
            root: true,
            run: function() {
                //Grid isimli maptype sınıf içeriği tanımlamaları
                defaults.push('.grid.this, .grid.in>* {display:inline-block; vertical-align:top; margin-right:-4px;}')
            }
        },
        //Bu method, başlangıçta yüklenmez. Bu method ilgili ekran boyutlarına göre her birine yüklenecek genel sınıfları barındırır
        //Örneğin parametre olarak name alanı "web" geldiğinde bu alan için web-hidden sınıfı belirlenerek ilgili karşılığı aktarılır
        //Aynı şekilde gelen name adı "mob" ise mob-hidden yada mob-show diye devam eder.
        //Her bir ekran boyutunda olması gereken ortak isim, tanımlamaları ve özelliklerini burada belirliyoruz
        withSizeName: {
            init: false,
            run: function(result, name) {

                var allDefaults = {
                    'hidden': 'display:none',
                    'show': 'display:block',
                    'textcenter': 'text-align:center',
                    'textright': 'text-align:right',
                    'textleft': 'text-align:left',
                    'left': 'margin-left:0; margin-right:auto;',
                    'center': 'margin-left:auto; margin-right:auto; display:inherit;',
                    'right': 'margin-left:auto; margin-right:0'
                };

                for (var n in allDefaults) {
                    result.push(format(".{0}-{1}{{2}}", name, n, allDefaults[n]));
                }

            }
        },
        //Otomatik çalıştırılır. Bu alanda belirtilen tüm sınıf tanımlamaları, ekran boyutlarından bağımsız olarak genel kullanım için tanımlanır
        //Herhangi bir ekran boyutuna bağımlı olmadığı için her boyutta belirtilen değerler kullanılabilecektir.
        //Ekran boyutlarından bağımsız değerler bu alanda belirtilmelidir.
        common: {
            init: true,
            run: function() {
                defaults.push('.nowrap{white-space:nowrap;}');
            }
        }

    };


    //Expression sınıfı bizim "in,this,all" özelliklerini kullanabilmemizi sağlamaktadır.
    var expression = (function() {

        //İlgili sınıfımız referans gösteriliyor
        var _this = this;

        //in,this özelliklerinin alması gereken sınıf değerlerini bu method ile dönüştürüp, elde ediyoruz
        //method içerisine istediğimiz formatı gönderip, bize bu formatta sınıf değerleri üretmesini istiyoruz 
        _this.expressionFormating = function(formating) {
            var n = [];
            screenSizes.for(function(key, item, index) {
                for (var x = 1; x <= currentPiece; x++) {
                    n.push(format(formating, key, x));
                }
            });
            return n;
        }

        //Gelen iki array nesnesi içerisindeki aynı değerleri bulur
        //Bulunan ortak değerler, referans verilen nesnenin alacağı sınıf içerik bilgisini döndürür
        _this.getOnlySameNames = function(arr, arr2) {
            var result = [];
            arr.forEach(function(e) {
                if (arr2.indexOf(e) != -1)
                    result.push(e);
            });
            return result;
        }

        //in ve this expression özelliklerinin ekran boyutlarına göre alabilecekleri minimum sınıf değerlerini tutar
        _this.expressionType = {
            'in': _this.expressionFormating('{0}-{1}'),
            'this': _this.expressionFormating('{0}{1}'),
            'all': false
        };

        //in,this,all vs gibi expressiontype minumum değerlerine dışarıdan erişerek almak için...
        //... sınıfımız içerisinde expression isimleriyle aynı olacak şekilde nesne listesi oluşturuyoruz
        //... yine bu döngü esnasında ilgili expression adı sınıf listesi adı olarak da ekleniyor
        Object.keys(_this.expressionType).forEach(function(fx) {
            if (_this.expressionType[fx]) {
                _this.expressionType[fx].push(fx);
                _this[fx] = _this.expressionType[fx].concat(mapTypeName);
            }
        });

        //Özel tanım. all dendiği anda in ve this tiplerinin aldığı tüm sınıf adlarını hafızada tutar
        _this.all = mapTypeName.slice(0);
        _this.expressionType.for(function(key, value, index) {
            if (value)
                _this.all = _this.all.concat(value);

        });

        //Sınıfımızın özelliklerinin dışarıdan erişilebilir olması için geriye döndürüyoruz
        return _this;

    })()


    //Reference Sınıfı ile sayfa üzerinde referans veren nesneler bulunup....
    //...referans verilen ilgili nesnelere belirtilen değerler aktarılmaktadır.

    var reference = (function() {

        //İçinde bulunulan sınıfı işaretliyoruz
        var _this = this;

        //var getallReferences = null;

        //Referans sınıfına dışardan erişilebilir referenceDatas adında bir property oluşturuyoruz
        //Bu alan içerisinde, sayfada referans veren nesnelerin referans verdikleri sınıf adı ve ilgili sınıf isimleri tutulmaktadır
        _this.referenceDatas = [];

        //contructor metodu
        function reference() {

            //ExpressionType değerleri adedince bir döngü oluşturuluyor ve expression isimlerine göre referanslar aranıyor
            //Örnegin ref-in, ref-this, ref-all, ref-?????? gibi devam edebilir.
            Object.keys(expression.expressionType).forEach(function(type) {

                //Öncelikle nesnelerde arayabileceğimiz maksimum ölçüde, benzersiz sınıf değerine sahip nesneler bulunmaktadır.
                var query = 'ref-' + type + '-';
                var result = d.querySelectorAll('[class*="' + query + '"]');
                var match = null;

                //Gelen tüm referans veren nesnelerin sayısında döngü oluşturuyoruz
                for (var n = 0, f; n < result.length; n++) {

                    //Referans veren ilgili nesnemizin class değerleri içerisinde, akış esnasındaki expressiontype değerine ait sınıflar karşılaştırılmaktadır
                    //expressiontype değerinde olan sınıf isimlerinin, ilgili nesne sınıfları arasında bulunanlar filtrelenmektedir.
                    //örnek çıktı : map in web-4 mob-6
                    f = expression.getOnlySameNames(result[n].classList, expression[type])

                    //Referans veren sıradaki nesne sınıfları içinden, referans verdiği adı alalım
                    //örnek : ref-in-benimsinifim
                    //Burada "benimsinifim" adli değer alınmaya çalışılmaktadır.
                    var reg = new RegExp(query + '([^\\s]\\w+)', 'g');
                    match = reg.exec(result[n].classList.value);

                    if (match) {

                        //İlgili nesneden gelen ref-xx-xx tanımını silelim
                        result[n].classList.remove(match[0]);

                        //Bulunan referans adları ve içeriği, daha sonra tekrar kullanılmak üzere hafızaya alıyoruz
                        //Örneğin sayfa üzerinde sonradan oluşacak div,span vs gibi elementlerin kontrolü sağlanarak
                        //referans listesinde referans verilmiş isimler bulduğumuzda rahatça bu nesnelere de uygulayabiliriz.
                        _this.referenceDatas.push({ key: match[1], value: f });

                        //Referans bilgileri alınırken, sayfa üzerinde aynı anda tarama yaparak referans verilen nesnelere ilgili değerleri atıyoruz
                        //Burada şimdilik <script></script> nesnemisin sayfanın en altında olduğunu varsayarak oluşturduk
                        //Ancak şuan için head veya body alt kısmında olması farketmiyor. frameworkümüzün içine onload özelliği ekli durumda
                        //Eğer daha sonradan onload özelliğini kapatıp, body alt alanına eklenmesi zorunlu kılarsak, buradaki işleyiş daha mantıklı olacaktır.
                        //Aksi takdir de <script></script> dosyamız head kısmına eklenir ve onload özelliği olmazsa sıkıntı çıkacaktır ve buradaki işleyiş hata verecektir.
                        var objs = document.querySelectorAll('.' + match[1]);

                        //Referans alanların sayısı kadar döngü oluşturuluyor
                        for (var x = 0; x < objs.length; x++) {
                            //Dinamik olması amacıyla apply özelliğini Reference sınıf için dinamik verdikleri
                            //apply methodu ile ilgili nesneye ilgili özellikler yüklenmektedir
                            _this.apply(match[1], objs[x], f);
                        }

                    }

                }

            });

            /*//Referanslar varsa sayfa üzerinde, şimdi hepsini referans gösterilen ilgili nesnelere yükleyelim
            if (_this.referenceDatas.length > 0) {
                getallReferences = document.querySelectorAll('.' + match[1]);
                for (var x = 0; x < getallReferences.length; x++) {
                    getallReferences[x].classList.remove(match[1]);
                    getallReferences[x].classList.concat()
                }
            }
            */


        }

        //ilgilili referans verilen nesneye belirtilen sınıf özellikleri yükleniyor
        _this.apply = function(cls, obj, values) {
            values.forEach(function(n) {
                obj.classList.add(n);
            });
            //Referans adı nesneden siliniyor
            obj.classList.remove(cls);
        }

        //Reference sınıfımızın dışarıdan erişilebilir özelliklerini kullanabilmek için, sınıfı tekrar geri döndürüyoruz
        return reference;

    })();




    var init = function() {

        //Tüm maptype'ler ve genel kullanımlar için init özelliği true olanları çalıştırır.
        //Bu şekilde sayfada kullanılacak maptype'lerın her biri içi zorunlu sınıflar karşılıkları hazırlanır. 
        mapTypes.for(function(key, item, index) {
            if (item.init)
                item.run();
        });

        //Her bir ekran boyutu için media dosyaları burada hazırlanır
        //Gelen her bir media boyutu parca sayısına göre iç sınıf değerleri şekillendirilir
        //Örneğin 12 parça ise 100/12 = her bir parça 8.3333% lik dilime sahiptir.
        for (var n in screenSizes) {
            defaults.push(media(screenSizes[n], newContent(currentPiece, n)));
        }

        //Sayfadaki referansları yükle
        reference();

        //Maptypes, media ve genel kullanım sınıflarının içeriklerinin yükleneceği style element nesnesi oluşturuluyor
        //Oluşturulan nesne, document.head alanına aktarılıyor
        var style = document.createElement('style');
        style.innerHTML = defaults.join('\n');
        d.head.appendChild(style);

    }

    //Her bir ekran boyut içeriğinin oluşturulduğu method
    //Örneğin gelen 800px'lik media screen içeriği %'lik değerleri burada oluşturuluyor
    function newContent(i, name) {
        var result = ['\n'];

        for (var n = 1; n <= i; n++) {

            mapTypes.for(function(key, obj, index) {

                var x = [];

                //alt nesneleri kapsaması isteniyorsa
                if (obj.children)
                    x.push(format(".{0}.in[class*='{1}-{2}']>*", key, name, n));

                //ilgili nesnenin kendisinin kapsanması isteniyorsa
                if (obj.root)
                    x.push(format(".{0}.this[class*='{1}{2}']", key, name, n));

                //root ve children değerlerine göre herhangi bir tanımlama yapılmışsa ekle diyoruz
                if (x.length > 0)
                    result.push(format("{0}{ width:{1}% }", x.join(','), calc(i, n)));

                //Hafızada bir şey tutma sil
                x = null;


            });


        }

        //ilgili boyutun media screen özelliğindeki aynı sınıf adlarına sahip verileri de yüklüyoruz
        //örneğin web-hidden, web-show vs her bir ekran boyutunda olması gereken ama kendine özgü sınıf tanımlamaları
        mapTypes.withSizeName.run(result, name);

        return result.join('\n');
    }

    function media(maxWidth, content) {
        if (maxWidth <= 0) return content;
        var result = format("\n\n@media screen and (max-width:{0}px){{1}}", maxWidth, content);
        return result;
    }

    function calc(i, n) {
        return (100 / i * n).toFixed(4);
    }

    function format() {
        var arg = arguments;
        for (var n = 1, result = arg[0]; n < arg.length; n++) {
            var z = new RegExp('\\{' + (n - 1) + '\\}', 'g');
            result = result.replace(z, arg[n]);
        }
        return result;
    }

    if (w.addEventListener)
        w.addEventListener('load', init, false);
    else
        w.attachEvent('onload', init);

    return init;

})(document, window);